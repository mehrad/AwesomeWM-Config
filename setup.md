install the following packages:

```
awesome
xautolock
picom
nitrogen
slock
nerd-fonts-complete

networkmanager
network-manager-applet

#lain
#awesome-freedesktop-git
```

Then either install the following or compile from source with required patches:

```
dmenu + Password patch
```
