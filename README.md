# Setup Environment

In this file I explain how to setup myown dwm environment. Remember that this is not just dwm and it has some other tools to make the environment (e.g dmenu)

## install pre-requirements for DWM

```sh
pamac update
pamac install awesome

# install dependencies for Rust apps
pamac install rustup
rustup install stable
rustup default stable

pamac install alacritty

pamac install slock
pamac install libx11
pamac install libxinerama
pamac install libxft
pamac install freetype2
pamac install dmenu # to get the -P flag for getting passwords, one should get dmenu and patch it manually
pamac install dunst
pamac install nitrogen
pamac install flameshot
pamac install nerd-fonts-complete
```


## install optional softwares

```sh
# install ohmyzsh
sh -c "$(curl -fsSL https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"

# install Neovim and required other required softwares
pamac install neovim
pamac install vim-plug



# pdf viewer
pamac install zathura

# terminal file manager
pamac install vifm

# ls alternative
pamac install exa

# install process manager
pamac install htop
pamac install ytop
```

## add AwesomeWM to the DM (Display Manager)

copy the following in `/usr/share/xsessions/awesome.desktop`

```
[Desktop Entry]
Name=awesome
Comment=Highly configurable framework window manager
TryExec=awesome
Exec=awesome
Type=Application
```
