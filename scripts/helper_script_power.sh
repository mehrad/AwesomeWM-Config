#!/bin/bash

# set the GUI program to type the password in (adding -A to sudo will run this)
export SUDO_ASKPASS="${HOME}/.config/dwm/dpass.sh"


echo "    [$(date +'%F %R')] The helper_script_power.sh was called." >> ~/.config/awesome/awesome.log

RET=$(echo -e "LockScreen\nLogout\nSuspend\nReboot\nShutdown\nCancel" | dmenu -i -fn "UbuntuMono Nerd Font:size=11" -nb "#222222" -nf "#ff7824" -sb "#ff7824" -sf "#222222")

echo "        [$(date +'%F %R')] The chosen item was: ${RET}" >> ~/.config/awesome/awesome.log

case $RET in
    LockScreen) xautolock -locknow ;;
    Logout) loginctl terminate-user "${USER}" ;;
    Suspend) xautolock -locknow ; systemctl suspend;;
	Reboot) systemctl reboot ;;
    Shutdown) systemctl poweroff ;;
	*) ;;
esac
